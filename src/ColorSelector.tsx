import React from 'react';
import { clamp, rgb_to_hsv, hsv_to_rgb, ColorRGB, ColorHSV } from './Util'

/**
 * Number input component, which combines a draggable slider and an input box.
 */
class NumberInput extends React.Component<
    { get: number, set: (n: number) => void, min: number, max: number, tooltip: string, hintGradient: string },
    { value: number, dragging: boolean }
> {
    myRef: React.RefObject<any>;

    /**
     * @param props Component properties.
     * @param props.get Current value.
     * @param props.set Function for setting current value.
     * @param props.min Minimum value of input.
     * @param props.max Maximum value of input.
     * @param props.tooltip What to show when hovering over the slider.
     * @param props.hintGradient Bottom border gradient. Useful for color selection.
     */
    constructor(props: any) {
        super(props);
        this.myRef = React.createRef();
        this.state = {
            value: props.get,
            dragging: false
        }
    }

    /**
     * Called when slider dragging starts.
     * @param e event object
     */
    onDragStart(e: React.DragEvent<HTMLInputElement>) {
        e.dataTransfer.setDragImage(new Image(), 0, 0)
        this.setState({ dragging: true });
    }

    /**
     * Called while slider is being dragged.
     * @param e event object
     */
    onDragEvent(e: React.DragEvent<HTMLInputElement>) {
        if (!e.clientX) {
            return;
        }
        let newValue = clamp(Math.round(e.nativeEvent.offsetX / (this.myRef.current.offsetWidth / this.props.max)),
            this.props.min,
            this.props.max);

        this.setState({ value: newValue })
    }

    /**
     * Called when slider dragging ends.
     * @param e event object
     */
    onDragEnd(e: React.DragEvent<HTMLInputElement>) {
        this.setState({ dragging: false });
        this.props.set(this.state.value);
    }

    /**
     * Renders the component.
     */
    render() {
        const { get, set, min, max, tooltip, hintGradient } = this.props;

        let currentValue = (this.state.dragging) ? this.state.value : get;

        let fraction = (currentValue - min) / max * 100
        return (
            <div className='stack numberOuter'>
                <input type='number'
                    className='number'
                    draggable
                    onDragStart={e => this.onDragStart(e)}
                    onDrag={e => this.onDragEvent(e)}
                    onDragEnd={e => this.onDragEnd(e)}
                    min={min}
                    max={max}
                    value={Math.round(currentValue)}
                    onChange={e => set(Number(e.target.value))}
                    ref={this.myRef}
                    style={{
                        background: `linear-gradient(to right, 
                        var(--number-front) 0%, 
                        var(--number-front) ${fraction}%, 
                        var(--number-back) ${fraction}%, 
                        var(--number-back) 100%)`,
                        borderLeft: `2px solid var(--number-front)`,
                        borderRight: `2px solid var(--number-back)`,
                    }}
                    title={tooltip}
                />
                <div
                    className='number'
                    style={{
                        background: `linear-gradient(to right, 
                    ${hintGradient}`,
                        position: `relative`,
                        top: `85%`,
                    }}
                >
                    &nbsp;
                </div>
            </div >
        )
    }
}

/**
 * Shows a list of preset colors.
 * @param props Component properties.
 * @param props.set Function for setting current color.
 * @param props.colors List of preset colors.
 * @returns Preset color selector component.
 */
function PresetColorMenu(props: {
    set: (c: ColorRGB) => void,
    colors: ColorRGB[]
}) {

    let i = 0;
    let buttons = props.colors.map(c => {
        return <div
            className="colorButton"
            key={i++}
            onClick={() => props.set(c)}
            title={`${i + 3}: ${c}`}
            style={{
                backgroundColor: `${c}`,
                borderColor: `${c.add(-50)}`,
                borderBottomColor: `${c.add(-130)}`,
                borderRightColor: `${c.add(-130)}`,
            }}
        >&nbsp;</div>
    }
    )
    return (
        <div className="popup down">
            <div className="flex-grid window popup-down">
                {buttons}
            </div>
        </div>
    )
}

/**
 * Renders a color selector, with both preset color options and direct HSV selection. 
 * @param props Component properties.
 * @param id Color ID.
 * @param label Selector label text.
 * @param get Current color value.
 * @param set Function for setting color value.
 * @param colors List of preset colors.
 * @param dropdownGet ID of color with current expanded preset menu.
 * @param dropdownSet Function for setting color with current expanded preset menu.
 * @returns Color selector component.
 */
export function ColorSelector(props: {
    id: string,
    label: string,
    get: ColorRGB,
    set: (c: ColorRGB) => void,
    colors: ColorRGB[],
    dropdownGet: boolean,
    dropdownSet: (id: string) => void
}) {

    let rgb: ColorRGB = props.get;
    let hsv = rgb_to_hsv(props.get);

    let hue = (hsv.s) ? hsv.h : rgb.h;
    let sat = (hsv.v) ? hsv.s : rgb.s;

    return (
        <div>
            <span className='label'>{props.label}</span>
            <div className='window flex-row' style={{ justifyContent: `space-between` }}>
                <div
                    className='number numberOuter'
                    style={{
                        backgroundColor: `${hsv}`,
                        width: `6rem`
                    }}
                    onClick={e => props.dropdownSet(props.id)}
                    title="Click for color presets"
                >
                    &nbsp;
                </div>
                <NumberInput
                    get={hue}
                    set={h => props.set(hsv_to_rgb(new ColorHSV(h, sat, hsv.v)))}
                    min={0}
                    max={360}
                    tooltip="Hue"
                    hintGradient="red, yellow, green, cyan, blue, magenta" />
                <NumberInput
                    get={sat}
                    set={s => props.set(hsv_to_rgb(new ColorHSV(hue, s, hsv.v)))}
                    min={0}
                    max={100}
                    tooltip="Saturation"
                    hintGradient={`gray, hsl(${hue}, 100%, 50%)`} />
                <NumberInput
                    get={hsv.v}
                    set={v => props.set(hsv_to_rgb(new ColorHSV(hue, sat, v)))}
                    min={0}
                    max={100}
                    tooltip="Value"
                    hintGradient={`black, white`} />
            </div>
            {(props.dropdownGet) ? <PresetColorMenu
                colors={props.colors}
                set={props.set}
            /> : null}
        </div>
    )
}
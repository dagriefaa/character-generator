
/**
 * Clamps x between min and max (i.e. [min, x, max]).
 * @param x Number to clamp
 * @param min Minimum value
 * @param max Maximum value
 * @returns Clamped value
 */
export function clamp(x: number, min: number, max: number) {
    if (min > max) {
        throw new RangeError(`${min} is greater than ${max}`)
    }
    if (x < min) {
        x = min;
    }
    else if (x > max) {
        x = max;
    }
    return x;
}

/**
 * RGB color representation object.
 */
export class ColorRGB {
    r: number;
    g: number;
    b: number;
    h: number;
    s: number;

    constructor(r: number, g: number, b: number, h: number = 0, s: number = 0) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.h = h;
        this.s = s;
    }

    /**
     * Returns a string representation of the color.
     */
    toString() {
        return `rgb(${this.r}, ${this.g}, ${this.b})`
    }

    /**
     * Adds a flat value to all components.
     * @param n Value to add
     * @returns Added color
     */
    add(n: number) {
        return new ColorRGB(this.r + n, this.g + n, this.b + n);
    }

    /**
     * Multiplies color by a value.
     * @param n Value to multiply
     * @returns Multiplied value
     */
    mul(n: number) {
        return new ColorRGB(this.r * n, this.g * n, this.b * n);
    }
}

/**
 * HSV color representation object.
 */
export class ColorHSV {
    h: number;
    s: number;
    v: number;

    constructor(h: number, s: number, v: number) {
        this.h = h;
        this.s = s;
        this.v = v;
    }

    /** Returns a string representation of the color (in RGB, as HSV does not exist in CSS) */
    toString() {
        let rgb = hsv_to_rgb(this);
        return `rgb(${rgb.r}, ${rgb.g}, ${rgb.b})`;
    }
}

/**
 * Converts color from RGB to HSV.
 * Adapted from easyrgb.com
 * @param x RGB
 * @returns HSV
 */
export function rgb_to_hsv(x: ColorRGB): ColorHSV {
    let var_R = (x.r / 255)
    let var_G = (x.g / 255)
    let var_B = (x.b / 255)

    let var_Min = Math.min(var_R, var_G, var_B)    //Min. value of RGB
    let var_Max = Math.max(var_R, var_G, var_B)    //Max. value of RGB
    let del_Max = var_Max - var_Min             //Delta RGB value

    let H = 0
    let S = 0
    let V = var_Max

    if (del_Max !== 0) {
        S = del_Max / var_Max

        let del_R = (((var_Max - var_R) / 6) + (del_Max / 2)) / del_Max
        let del_G = (((var_Max - var_G) / 6) + (del_Max / 2)) / del_Max
        let del_B = (((var_Max - var_B) / 6) + (del_Max / 2)) / del_Max

        if (var_R === var_Max) H = del_B - del_G
        else if (var_G === var_Max) H = (1 / 3) + del_R - del_B
        else if (var_B === var_Max) H = (2 / 3) + del_G - del_R

        if (H < 0) H += 1
        if (H > 1) H -= 1
    }
    return new ColorHSV(H * 360, S * 100, V * 100)
}

/**
 * Converts color from HSV to RGB (preserving hue and saturation).
 * Adapted from easyrgb.com
 * @param x HSV
 * @returns RGB
 */
export function hsv_to_rgb(x: ColorHSV): ColorRGB {
    let h = (x.h % 360) / 360;
    let s = x.s / 100;
    let v = x.v / 100;

    if (s === 0) {
        return new ColorRGB(v * 255, v * 255, v * 255, x.h);
    }
    else {
        let var_h = h * 6
        let var_i = Math.floor(var_h)
        let var_1 = v * (1 - s)
        let var_2 = v * (1 - s * (var_h - var_i))
        let var_3 = v * (1 - s * (1 - (var_h - var_i)))

        let var_r: number, var_g: number, var_b: number;

        if (var_i === 0) { var_r = v; var_g = var_3; var_b = var_1 }
        else if (var_i === 1) { var_r = var_2; var_g = v; var_b = var_1 }
        else if (var_i === 2) { var_r = var_1; var_g = v; var_b = var_3 }
        else if (var_i === 3) { var_r = var_1; var_g = var_2; var_b = v }
        else if (var_i === 4) { var_r = var_3; var_g = var_1; var_b = v }
        else { var_r = v; var_g = var_1; var_b = var_2 }

        return new ColorRGB(var_r * 255, var_g * 255, var_b * 255, x.h, x.s);
    }
}

/**
 * Sorts a dictionary object by its keys, alphabetically.
 * @param obj Object to sort
 * @returns Sorted object
 */
export function sorted(obj: { [key: string]: any }): { [key: string]: any } {
    return Object.keys(obj).sort().reduce(function (result: { [key: string]: any }, key) {
        result[key] = obj[key];
        return result;
    }, {});
}
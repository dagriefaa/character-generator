import React, { useRef, useEffect, useState } from 'react';
import './App.css';
import { ColorRGB, sorted } from './Util'
import { ColorSelector } from './ColorSelector';
import { ElementPaths } from './config/ElementPaths';

const NULL_ITEM = "!null"
const SAVE_DATA = "saved"

// Expected final image size
const IMAGE_W = 443
const IMAGE_H = 587

/* Webpack image/file object */
interface Module {
    default: string
}

/* Elements.json config object */
interface ElementConfig {
    label: string
    colorIds: string[]
    offset: { x: number, y: number }
    nullable: boolean
    default?: number
    thumbElements?: string[]
}

/* Colors.json config object */
interface ColorConfig {
    colors: string,
    label: string
}

/* Dictionary of parts for each element */
interface ElementParts {
    [element: string]: PartList
}

/* Contains parts for an element */
interface PartList {
    [part: string]: {
        [layer: number]: {
            src: string
            canColor: boolean
        }
    }
}

/* Object for storing current element state in local storage */
interface ElementDict {
    [key: string]: string
}

/* Object for storing current color state in local storage */
interface ColorDict {
    [key: string]: ColorRGB
}

const Elements: { [key: string]: ElementConfig } = require('./config/Elements.json');
const Categories: { [key: string]: string[] } = require('./config/Categories.json');
const Colors: { [key: string]: { [key: string]: ColorConfig } } = require('./config/Colors.json');

function ValidateConfigs() {
    let colors = Object.values(Colors).reduce((prevSection: string[], section) => prevSection.concat(Object.keys(section)), [])
    Object.keys(Elements).forEach(element => {
        Elements[element].colorIds.forEach(color => {
            if (!colors.includes(color)) {
                throw new ReferenceError(`Elements.json: Color id '${color}' on element '${element}' does not exist!`)
            }
        })

        Elements[element].thumbElements?.forEach(thumbElement => {
            if (!Object.keys(Elements).includes(thumbElement)) {
                throw new ReferenceError(`Elements.json: Thumb element id '${thumbElement}' on element '${element}' does not exist!`)
            }
        })
    })

    Object.values(Categories).forEach(elements =>
        elements.forEach(element => {
            if (!Object.keys(Elements).includes(element)) {
                throw new ReferenceError(`Categories.json: Element id '${element}' does not exist!`)
            }
        })
    );
}
ValidateConfigs();

const ColorPresets: { [key: string]: ColorRGB[] } = {
    "DyeColors": require('./colors/DyeColors.json').map((x: number[]) => new ColorRGB(x[0], x[1], x[2])),
    "SkinColors": require('./colors/SkinColors.json').map((x: number[]) => new ColorRGB(x[0], x[1], x[2]))
};

// keeps images loaded to reduce reloading times
const ImageCache: HTMLImageElement[] = []

function importAll(r: __WebpackModuleApi.RequireContext): Module[] {
    return r.keys().map(r) as Module[]; // webpack why
}

function ConstructPartsList(previous: ElementParts, current: string): ElementParts {

    let images = importAll(ElementPaths[current])

    let parts: PartList = {}

    if (Elements[current].nullable) {
        parts[NULL_ITEM] = []
    }

    images.forEach((module: Module) => {
        let name: string[] = module.default.toString().split('.')[0].split('_');

        let cache = new Image();
        cache.src = module.default;
        ImageCache.push(cache)

        let part = name[0];
        let layer = 0;
        let colorable = false

        if (name.length > 1) {
            layer = Number(name[1].match(/\d+/)?.[0] ?? 0);
            colorable = (name[1].match(/[c]/)?.length ?? 0) > 0;
        }

        if (!parts.hasOwnProperty(part)) {
            parts[part] = []
        }
        if (!parts[part].hasOwnProperty(layer)) {
            parts[part][layer] = {
                src: "",
                canColor: colorable
            }
        }
        parts[part][layer].src = module.default;
    })

    previous[current] = sorted(parts)
    return previous
}
const Parts: ElementParts = Object.keys(Elements).reduce(ConstructPartsList, {});

/**
 * Renders a single layer image for compositing.
 * @param props Component properties.
 * @param props.image Layer image path.
 * @param props.position Position offset of layer image on canvas.
 * @param props.color Tint color of image. Subtracts 130 from each RGB component, and adds the result to the image.
 * @param props.crop Crop canvas to the specified size in pixels.
 * @param props.scaleSize Scale canvas to fit within the specified size in pixels. Applied AFTER cropping.
 * @param props.onRender Called with the canvas object when rendering is complete. Used for compositing.
 * @returns Layer renderer component
 */
function LayerRenderer(props: {
    image: string,
    position: { x: number, y: number },
    color?: ColorRGB,
    crop?: { x: number, y: number, w: number, h: number },
    scaleSize?: { w: number, h: number },
    onRender?: (x: HTMLCanvasElement) => void
}): JSX.Element {

    const { image, position, color, scaleSize, crop, onRender } = props;
    const canvasRef: React.RefObject<HTMLCanvasElement> = useRef(null)

    let scale = 1;
    if (scaleSize) {
        scale = Math.min(
            scaleSize.w / ((crop) ? crop.w : IMAGE_W),
            scaleSize.h / ((crop) ? crop.h : IMAGE_H));
    }

    useEffect(() => {

        let canvas = canvasRef.current as HTMLCanvasElement;
        let context = canvas.getContext('2d') as CanvasRenderingContext2D;

        context.imageSmoothingEnabled = true;

        let imageElement = new Image();

        imageElement.onload = () => {

            context.clearRect(0, 0, canvas.width, canvas.height);
            context.globalCompositeOperation = 'source-over';
            context.drawImage(imageElement,
                (position.x - (crop?.x ?? 0)) * scale,
                (position.y - (crop?.y ?? 0)) * scale,
                imageElement.width * scale,
                imageElement.height * scale);

            // it's assumed that the base color of the image is (130,130,130)
            // so to set the color of the image, it subtracts 130 from each component
            // and then adds positive components, inverts, adds negative components, and inverts again
            // this way color subtraction can be done as well as color addition
            if (color) {
                let r = color.r - 130;
                let g = color.g - 130;
                let b = color.b - 130;
                let addColor = new ColorRGB(
                    Math.max(r, 0),
                    Math.max(g, 0),
                    Math.max(b, 0),
                );
                let subtractColor = new ColorRGB(
                    -Math.min(r, 0),
                    -Math.min(g, 0),
                    -Math.min(b, 0),
                )

                context.globalCompositeOperation = 'lighter';
                context.fillStyle = `${addColor}`
                context.fillRect(0, 0, canvas.width, canvas.height);

                context.globalCompositeOperation = 'difference';
                context.fillStyle = `white`
                context.fillRect(0, 0, canvas.width, canvas.height);

                context.globalCompositeOperation = 'lighter';
                context.fillStyle = `${subtractColor}`
                context.fillRect(0, 0, canvas.width, canvas.height);

                context.globalCompositeOperation = 'difference';
                context.fillStyle = `white`
                context.fillRect(0, 0, canvas.width, canvas.height);

                context.globalCompositeOperation = 'destination-in';
                context.drawImage(imageElement,
                    (position.x - (crop?.x ?? 0)) * scale,
                    (position.y - (crop?.y ?? 0)) * scale,
                    imageElement.width * scale,
                    imageElement.height * scale);
            }
        };

        imageElement.src = image;

        if (onRender) {
            onRender(canvas);
        }

    })

    return <canvas
        ref={canvasRef}
        width={scaleSize?.w ?? ((crop) ? crop.w : IMAGE_W)}
        height={scaleSize?.h ?? ((crop) ? crop.h : IMAGE_H)}
    />
}

/**
 * Renders tabs for selecting the element to display parts for.
 * @param props Component properties.
 * @param props.get Current value (i.e. tab).
 * @param props.set Function for setting current value.
 * @returns Part tabs component.
 */
function ElementTabs(props: {
    get: string,
    set: (s: string) => void
}): JSX.Element {

    let categories = Object.keys(Categories).reduce((prevSection: JSX.Element[], section) =>
        prevSection.concat(
            (<span key={`tabs-${section}`} className='label header'>{section}</span>),
            Categories[section].reduce((prevCategory: JSX.Element[], category) =>
                prevCategory.concat(<div key={category}
                    className={`button category ${(category === props.get) ? "selected" : ""}`}
                    onClick={e => props.set(category)}
                >
                    {Elements[category].label}
                </div>)
                , [])
        ), []);

    return (
        <div className="flex-column scroll categoryList"
            style={{ gridColumn: `1`, }}
        >
            {categories}
        </div>
    )
}

/**
 * Renders a button for selecting a part, with dynamic thumbnail.
 * @param props Component properties.
 * @param props.element Element ID.
 * @param props.part Part ID.
 * @param props.get Current element state.
 * @param props.set Function for setting current element state.
 * @param props.colors Current color state.
 * @returns Button for selecting parts.
 */
function PartButton(props: {
    element: string,
    part: string,
    get: { [key: string]: string },
    set: (es: { [key: string]: string }) => void
    colors: { [key: string]: ColorRGB }
}): JSX.Element {
    const { element, part, get, set, colors } = props;

    // 'intelligent' thumbnail cropping - displays only the relevant area of the canvas
    let { x, y } = Elements[element].offset;
    x = Math.min(x * 0.9, (IMAGE_W * 0.5) - 50)
    let w = IMAGE_W - (x * 2)
    let h = w * 1.22
    y = Math.min(y * 0.9, (IMAGE_H * 0.5), IMAGE_H - h)
    let crop = { x: x, y: y, w: w, h: h }

    let id = 0;
    let render = (currentElement: string, currentPart: string) => {
        let colorLayer = 0;
        return Object.values(Parts[currentElement][currentPart]).reduce((prevParts: JSX.Element[], part) =>
            [<LayerRenderer
                key={`${id++}`}
                color={part.canColor ? colors[Elements[currentElement].colorIds[colorLayer++]] : undefined}
                image={part.src}
                position={Elements[currentElement].offset}
                crop={crop}
                scaleSize={{ w: 160, h: 160 * 1.23 }}
            />].concat(prevParts), [])
    }

    let elementIndex = Object.keys(Elements).indexOf(element)
    let prevElementIds = Elements[element].thumbElements?.filter(x => Object.keys(Elements).indexOf(x) < elementIndex) ?? [];
    let nextElementIds = Elements[element].thumbElements?.filter(x => Object.keys(Elements).indexOf(x) > elementIndex) ?? [];

    let layers: JSX.Element[] =
        (prevElementIds.reduce((prevElements: JSX.Element[], element) => prevElements.concat(render(element, get[element])), []))
            .concat(render(element, part))
            .concat(nextElementIds.reduce((prevElements: JSX.Element[], element) => prevElements.concat(render(element, get[element])), []))

    return (
        <div className={`stack button frame part ${(get[element] === part) ? "selected" : ""}`}
            onClick={e => {
                let es = { ...get };
                es[element] = part;
                set(es);
            }}
        >
            {layers}
        </div>
    )
}

/**
 * Renders the part selection dialog, including current element tabs and part buttons.
 * @param props Component properties.
 * @param props.get Current element state.
 * @param props.set Function for setting current element state.
 * @param props.colors Current color state.
 * @returns Part selection panel
 */
function PartDisplay(props: {
    get: { [key: string]: string },
    set: (es: { [key: string]: string }) => void
    colors: { [key: string]: ColorRGB }
}) {
    const { get, set, colors } = props;
    const [tab, setTab] = useState(localStorage.getItem('currentTab') ?? Object.values(Categories)[0][0])

    let partId = 0;
    let parts = Object.keys(Parts[tab]).map(part => (<PartButton
        key={partId++}
        element={tab}
        part={part}
        get={get}
        set={x => set(x)}
        colors={colors}
    />))

    return (
        <div id="parts"
            style={{
                display: `grid`,
                justifyContent: `start`,
            }}>
            <ElementTabs get={tab} set={x => {
                setTab(x);
                localStorage.setItem('currentTab', x)
            }} />

            <div className="flex-grid partList scroll" style={{ gridColumn: `2` }}>
                {parts}
            </div>
        </div>

    )

}

/**
 * Renders the color selectors for each layer.
 * @param props Component properties
 * @param props.get Current color state
 * @param props.set Function to set color state
 * @param props.show Color IDs to render (i.e. are being used for the current selected parts)
 * @returns Color selector list
 */
function ColorDisplay(props: {
    get: { [key: string]: ColorRGB },
    set: (cs: { [key: string]: ColorRGB }) => void,
    show: string[]
}) {

    const { get, set, show } = props;
    const [opened, setOpened] = useState("");
    const [lastShow, setLastShow] = useState(show);

    if (lastShow !== show) {
        setOpened("");
        setLastShow(show);
    }

    let colorInputs = Object.keys(Colors).reduce((prevSections: JSX.Element[], section) =>
        prevSections.concat(
            (<span key={`color-${section}`} className='label header'>{section}</span>),
            Object.keys(Colors[section])
                .filter(x => show.includes(x))
                .reduce((prevColors: JSX.Element[], id) => prevColors.concat(
                    <ColorSelector
                        key={id}
                        id={id}
                        label={Colors[section][id].label}
                        get={get[id]}
                        set={(c: ColorRGB) => {
                            let cs = { ...get };
                            cs[id] = c;
                            set(cs);
                        }}
                        colors={ColorPresets[Colors[section][id].colors]}
                        dropdownGet={opened === id}
                        dropdownSet={x => setOpened((opened !== x) ? x : "")}
                    />
                ), [])
        ), []);

    return (
        <div id="colors" className="flex-column scroll">
            {colorInputs}
        </div>
    )

}

/**
 * Renders save/load and download options for the current character.
 * @param props Component properties.
 * @param props.loadedName Name of most recently loaded character
 * @param props.save Function for saving a character (with name) to local storage.
 * @param props.load Function for loading a character (with name) from local storage.
 * @param props.remove Function for deleting a character (with name) from local storage.
 * @param props.render Function for compositing and downloading the character image.
 * @returns Save display component
 */
function SaveDisplay(props: {
    loadedName: string
    save: (x: string) => void
    load: (x: string) => void
    remove: (x: string) => void
    render: (x: string) => void
}) {

    const { loadedName, save, load, remove, render } = props
    const [name, setName] = useState("")
    const [prevName, setPrevName] = useState("")
    const [showSaved, setShowSaved] = useState(false)

    // replace contents of name field with loaded character on load
    if (loadedName !== prevName) {
        setName(loadedName)
        setPrevName(loadedName)
    }

    let saveData = JSON.parse(localStorage.getItem(SAVE_DATA) as string);
    let savedItems: JSX.Element[] = Object.keys(saveData).map(x => (
        <div key={x} className="flex-row">
            <div className="button" style={{ flexGrow: 1 }} onClick={e => load(x)}>{x}</div>
            <div className="button" title="Save" onClick={e => save(x)}>💾</div>
            <div className="button" title="Delete" onClick={e => remove(x)}>🗑</div>
        </div>
    ))

    return (
        <div className="frame"
            style={{
                maxWidth: `calc(0.4rem + ${IMAGE_W}px)`,
                padding: `1rem`,
                marginTop: `1rem`,
            }}>
            <div className="flex-row"
                style={{ marginBottom: `0.5rem` }}>
                <input type="text"
                    placeholder="Name"
                    value={name}
                    onChange={x => setName(x.target.value)}
                />
                <div className="button nowrap" onClick={e => save(name)}>💾 Save</div>
                <div className="button nowrap" onClick={e => render(name)}>📩 Download</div>
            </div>
            {(!showSaved) ? null : (
                <div className="popup up">
                    <div className="window flex-column"
                        style={{
                            maxHeight: `30rem`,
                            overflowY: `scroll`,
                        }}>
                        {savedItems}
                    </div>
                </div>
            )}
            <div className="button" onClick={e => setShowSaved(!showSaved)}>📂 Show Saved Items</div>

        </div>
    )
}

/**
 * Main character image composite display.
 * @param props Component properties.
 * @param props.elements Current element state.
 * @param props.colors Current color state.
 * @param props.onRender Function called when a layer finishes rendering. Used for compositing.
 * @returns Image composite component
 */
function CompositeDisplay(props: {
    elements: ElementDict
    colors: ColorDict
    onRender: (elementId: string, layer: HTMLCanvasElement) => void
}) {

    const { elements, colors, onRender } = props

    let elementStack = Object.keys(elements).reduce((prevElements: JSX.Element[], element) => {
        let colorLayer = 0;
        let layerId = 0;
        return prevElements.concat(
            Object.values(Parts[element][elements[element]])
                .reduce((prevParts: JSX.Element[], part) =>
                    [<LayerRenderer
                        key={`${element}${layerId++}`}
                        color={part.canColor ? colors[Elements[element].colorIds[colorLayer++]] : undefined}
                        image={part.src}
                        position={Elements[element].offset}
                        scaleSize={{ w: IMAGE_W, h: IMAGE_H }}
                        onRender={layer => onRender(`${element}${layerId++}`, layer)}
                    />].concat(prevParts), [])
        )
    }, [])

    return (
        <div className="frame stack">
            {elementStack}
        </div>
    )

}

/**
 * Main component for the application.
 * @returns The entire interface.
 */
export default function App() {

    let defaultElements = () => {
        let res: ElementDict = {};
        Object.keys(Elements).forEach(x =>
            res[x] = Object.keys(Parts[x])[Elements[x].default ?? 0]
        );
        return res;
    }

    let defaultColors = () => {
        let res: ColorDict = {};
        Object.values(Colors).forEach(x =>
            Object.keys(x).forEach(y => res[y] = ColorPresets[x[y].colors][2])
        );
        return res;
    }

    let [elements, setElements] = useState(defaultElements())
    let [colors, setColors] = useState(defaultColors())
    let [name, setName] = useState("");
    let [update, setUpdate] = useState(0);

    let layers: { [key: string]: HTMLCanvasElement } = {};

    if (!localStorage.getItem(SAVE_DATA)) {
        localStorage.setItem(SAVE_DATA, JSON.stringify({}))
    }

    let forceUpdate = () => {
        setUpdate(update + 1);
    }

    // Saves current state to local storage
    let save = (x: string) => {
        if (x === "") { return; }
        let saveData = JSON.parse(localStorage.getItem(SAVE_DATA) as string)
        saveData[x] = { elements: elements, colors: colors };
        localStorage.setItem(SAVE_DATA, JSON.stringify(saveData))
        forceUpdate();
    }

    // Loads state from local storage
    let load = (x: string) => {
        let saveData: { [key: string]: { elements: ElementDict, colors: ColorDict } } = JSON.parse(localStorage.getItem(SAVE_DATA) as string)
        setElements({ ...elements, ...saveData[x].elements });
        setColors({ ...colors, ...saveData[x].colors });
        setName(x)
    }

    // Removes a saved state from local storage
    let remove = (x: string) => {
        let saveData = JSON.parse(localStorage.getItem(SAVE_DATA) as string)
        delete saveData[x];
        localStorage.setItem(SAVE_DATA, JSON.stringify(saveData))
        forceUpdate();
    }

    // Renders the character to an image and downloads it
    let renderToImage = (name: string, layers: { [key: string]: HTMLCanvasElement }) => {
        let canvas = document.createElement('canvas');
        let context = canvas.getContext('2d') as CanvasRenderingContext2D;
        let count = 0;
        Object.values(layers).forEach(x => {
            canvas.height = Math.max(x.height, canvas.height);
            canvas.width = Math.max(x.width, canvas.width);
            let image = new Image();
            image.onload = () => {

                context.drawImage(image, 0, 0)

                if (++count === Object.keys(layers).length) {
                    let link = document.createElement('a');
                    link.download = `${name}.png`;
                    link.href = canvas.toDataURL()
                    link.click();
                }
            }
            image.src = x.toDataURL();
        })
    }

    return (
        <div id="main">
            <PartDisplay
                get={elements}
                set={es => setElements(es)}
                colors={colors}
            />

            <div id="composite" className="flex-column"
                style={{
                    alignItems: `center`,
                    padding: `1rem`,
                }}>
                <CompositeDisplay
                    elements={elements}
                    colors={colors}
                    onRender={(elementId, layer) => layers[elementId] = layer}
                />

                <SaveDisplay
                    loadedName={name}
                    save={x => save(x)}
                    load={x => load(x)}
                    remove={x => remove(x)}
                    render={(name) => renderToImage(name, layers)}
                />

            </div>

            <ColorDisplay
                get={colors}
                set={cs => setColors(cs)}
                show={Object.keys(elements)
                    .filter(x => elements[x] !== NULL_ITEM)
                    .map(x => Elements[x].colorIds.slice(0,
                        Object.values(Parts[x][elements[x]]).filter(x => x.canColor).length
                    ))
                    .flat()}
            />
        </div>
    );
}

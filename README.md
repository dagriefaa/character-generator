<div align="center">
<img src="https://i.imgur.com/worPZmF.png" alt="Character Generator" width="500"/><br>
https://dagriefaa.gitlab.io/character-generator
</div>
<br>

[[_TOC_]]

## Features
* Assemble and color characters (or anything, really) from provided part images
* Save/load characters to/from local storage
* Download character images to your computer
* Additive/subtractive color blending - ensures excellent contrast and detail no matter what color you choose
* Dynamic part thumbnails - automatically generates a great thumbnail based on part size and position
* Dynamic part loading - once the element is set up, just put the images in the right place with the right name
* Highly configurable - every color, element, and part can be replaced and arranged as you see fit

## Technologies
* [Typescript](https://www.typescriptlang.org/) - fixes all of the problems with dynamic typing... most of the time
* [React](https://reactjs.org/) - client-side UI construction
    * [Create React App](https://facebook.github.io/create-react-app) - bootstrapping

## Installation
1. Clone this repo to wherever you plan to store it (e.g. your desktop, a server, etc.).
2. Run `npm install` to install all the dependencies.
3. Run `npm start` to start the server. It will run on port 3000.

## Configuration
The system uses external definitions for (almost) everything in JSON, allowing (relatively) easy addition of new layers and colors without having to dig through the source code.

These files are found in `./src/config/`.

Each 'element' uses a 'part'. Each 'part' is made up of 'layers', which can each be colored. <br>
The structure of the part is defined by the name of its layer images: <br>
`<id>_<layer><c>.png`
* id: The part id.
* layer: Which layer the image belongs to. Layers are composited in descending order. Defaults to 0.
* c: Add `c` to specify the image can be colored.

### Elements.json
Layer ID file. Each layer is defined sequentially in the order they should be composited in.

```ts
[id: string] {
    label: string
    colorIds: string[]
    offset: { x: number, y: number }
    nullable: boolean
    default?: number
    thumbElements: string[]
}
```
* **id**: The **element id**.
* **label**: The element's name. Shown in the part selector.
* **colorIds**: List of **color ids**. Used sequentially by colorable layers.
* **offset**: Pixel offset of the image relative to the base canvas.
* **nullable**: Whether the element has an 'empty' option.
* **default**: (Optional) The index of the default part to use. Defaults to 0.
* **thumbElements**: (Optional) Other elements to include in the part thumbnail. Used by the part selector.

### ElementPaths.tsx
Maps element IDs to folders of part images. 

Because of how webpack is designed, it only allows literals to be used for `require` parameters, so this is a block of TypeScript code. Don't touch the first line.
```ts
[id: string]: require.context(path: string, false)
```
* id: The **element id**.
* path: The **relative** path to the part image folder for this id. 

A part containing three layers might look like this:
* `09_c.png`
* `09_1.png`
* `09_2c.png`

### Colors.json
Color ID/list definition file. Defines color ids and the structure of the color selector.
```ts
[section: string]: {
    [colorId: string]: {
        colors: ["DyeColors", "SkinColors" ],
        label: string
    }
}
```
* section: The section name. Displayed in the color selector.
* colorId: The **color ID**.
* colors: The file to use for the color's presets.
* label: The color name. Displayed in the color selector.

### Categories.json
Layer list definition file. Defines the structure of the layer list in the part selector.
```ts
[section: string]: string[]
```
* section: The section name. Displayed in the part selector.
    * Contains **element ids** in that section.

## Usage
The system is separated into three distinct sections.

<div align="center"><img src="https://i.imgur.com/eauzGfR.png" alt="Screenshot of app" width="700"/></div>

### Part Selector
The Part Selector allows selecting the parts you want for each element. <br>
* Select an element from the tabs on the left.
* Select parts for that element from the tiles on the right.

### Output Panel
The Output Panel displays the composited image and a control panel for file operations.
* Set the name of the character with the text box.
* Save the character to local storage with the 'Save' button. Requires the name to not be empty.
* Download the character image with the 'Download' button.
* Show the characters saved in local storage with the 'Show Saved Items' button. You can load, overwrite, or remove items here.

### Color Selector
The Color Selector allows selecting the colors (and sub-colors) for each layer. <br>
* Colors are specified in HSV.
* Clicking on the color preview will open up a dropdown to select preset colors.
* The number inputs are also sliders - drag on them to change the value quickly.

## Status
Development of this system is complete (from a functional standpoint). <br>
However, minor things and additional parts might be added in the future.

* Highlight layers when hovering over colors or element tabs

## Licensing
The code for this project is licensed under the MIT license.

The included artwork was derived from images created by [PrinceOfRedRoses](www.deviantart.com/princeofredroses) for a character generator on RinmaruGames.com, which has since become defunct. <br>
These assets are for demonstration purposes. Character images generated using them are for non-commercial use only.
